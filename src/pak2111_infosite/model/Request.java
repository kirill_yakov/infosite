package pak2111_infosite.model;

public class Request {
	
	private int id;
	private String operTime;
	private String user;
	// 1-Waiting, 2-Denied, 3-Approved
	private String status;
	private String type;

	public Request(int id, String operTime, String user, String status, String type) {
		super();
		this.id = id;
		this.operTime = operTime;
		this.user = user;
		this.status = status;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOper_time() {
		return operTime;
	}

	public void setOper_time(String oper_time) {
		this.operTime = oper_time;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return id +"\t | " + operTime + "\t | " + user
				+"\t | " + status + "\t | " + type;
	}
}
