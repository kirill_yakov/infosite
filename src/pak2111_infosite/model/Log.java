package pak2111_infosite.model;

public class Log {
	
	private int id;
	private String operTime;
	private String user;
	private String type;
	private String detail;
	
	public Log(int id, String operTime, String user, String type, String detail) {
		super();
		this.id = id;
		this.operTime = operTime;
		this.user = user;
		this.type = type;
		this.detail = detail;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOperTime() {
		return operTime;
	}
	public void setOperTime(String operTime) {
		this.operTime = operTime;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return id +"\t | " + operTime + "\t | " + user
				+"\t | " + type + "\t | " + detail;
	}	
}
