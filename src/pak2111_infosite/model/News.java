package pak2111_infosite.model;

public class News {
	private int id;
	private String newsTitle;
	private String newsText;
	private String author;
	private String newsDate;
	final String line="\n"+"********************************************************************************"+"\n";
	
	public News(int id,String newsTitle, String newsText, String author,
			String newsDate) {
		this.id=id;
		this.newsTitle = newsTitle;
		this.newsText = newsText;
		this.author = author;
		this.newsDate = newsDate;
	}
	
	public String outputText(String line,int width){
		String formattedLine="";
		for (int i=0;i<line.length();i++){
			if ((i+1)%(width)==0) formattedLine=formattedLine+"\n";
			formattedLine=formattedLine+line.charAt(i);
		}
		return formattedLine;
	}
	
	public String outputTextBreaking(String line,int width){
		String formattedLine="";
		int cntLine=1;
		for (int i=0;i<line.length();i++){
			formattedLine=formattedLine+line.charAt(i);
			if ((line.charAt(i)==' ')&&(line.indexOf(" ",i+1))>cntLine*width) {
				formattedLine=formattedLine+"\n";
				cntLine++;			
				}
			}					
		return formattedLine;
	}

	@Override
	public String toString() {
		
        return id+"\t | "+newsDate+"\t | "+author+"\t | "+newsTitle;
		
//		return line+newsDate+"\t\t\t\t\t"+author+"\n\n"+newsTitle+"\n\n"+outputTextBreaking(newsText,80);
		
//		return line+newsDate+"\t\t\t\t\t"+author+"\n\n"+newsTitle+"\n\n"+outputText(newsText,80)+line;		
		
//		return "News [newsTitle=" + newsTitle + ", newsText=" + newsText
//				+ ", author=" + author + ", newsDate=" + newsDate + "]";
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

	public String getNewsText() {
		return newsText;
	}

	public void setNewsText(String newsText) {
		this.newsText = newsText;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getNewsDate() {
		return newsDate;
	}

	public void setNewsDate(String newsDate) {
		this.newsDate = newsDate;
	}

	public String getLine() {
		return line;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
	
	
	
	

}
