package pak2111_infosite;

import pak2111_infosite.constants.*;
import pak2111_infosite.forms.Forms;
import pak2111_infosite.model.*;
import pak2111_infosite.utils.*;

public class Main {

	// Run Once Before first Launch
	static void databasePreOperations() throws Exception {
		DBUserOperations.createUserTable();
		DBNewsOperations.createNewsTable();
		DBLogOperations.createLogsTable();
		DBRequestOperations.createRequestTable();
		
		DBUserOperations.insertUserToDB(1, "Kirill", "1303","User");
		DBNewsOperations.insertNewsToDB(1, "IDEA 14", "...",
				"Kirill", "05.11.2014");
}
	
	public static void main(String[] args) throws Exception {
//		DBLogOperations.insertLogToDB(Forms.now(),"Anonymous", "MainMenu", "Start");
		Session currentSession = new Session();
		Forms.invitationMenuForm(currentSession);
	}
}
