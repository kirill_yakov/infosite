package pak2111_infosite.utils;

import pak2111_infosite.model.*; 

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class DBRequestOperations {

	static String dbPath = DBOperations.dbPath;
	static String dbName = DBOperations.dbName;
	static String tableRequest = "Request";

	// Create Table
	public static void createRequestTable() throws Exception {
		Class.forName("org.sqlite.JDBC");
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS '"
				+ tableRequest
				+ "' (ID INTEGER PRIMARY KEY AUTOINCREMENT,Oper_Time Text,User Text," +
				"Status Text,Type Text);");
		st.close();
		bd.close();
	}

	public static void insertRequestToDB(String operTime, String user, String status,
			String type) throws Exception {
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		PreparedStatement pst = bd.prepareStatement("INSERT INTO " + tableRequest
				+ " (Oper_Time,User,Status,Type) values (?,?,?,?);");
		pst.setString(1, operTime);
		pst.setString(2, user);
		pst.setString(3, status);		
		pst.setString(4, type);
		pst.executeUpdate();
		pst.close();
		bd.close();
	}

	public static ArrayList<Request> getRequestDataFromDB() throws Exception {
		ArrayList<Request> getRequest = new ArrayList<Request>();
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		ResultSet ss = st.executeQuery("SELECT * FROM " + tableRequest+
				" ORDER BY id DESC");
		while (ss.next()) {
			Request request=new Request(0,"","","","");
			request.setId(ss.getInt(1));
			request.setOper_time(ss.getString(2));
			request.setUser(ss.getString(3));
			request.setStatus(ss.getString(4));
			request.setType(ss.getString(5));
			getRequest.add(request);
		}
		ss.close();
		st.close();
		bd.close();
		return getRequest;
	}

	public static void printRequestFromDB(ArrayList<Request> getRequest) throws Exception {
		printRequestTableCaption();
		System.out.println();
		Iterator<Request> iterator = getRequest.iterator();
		Request request=new Request(0,"","","","");
		while (iterator.hasNext()) {
			request = (Request) (iterator.next());
			System.out.println(request);
		}
		System.out.println("TOTAL=" + DBOperations.lengthDB(tableRequest));
	}

	public static void printRequestTableCaption(){
		System.out.print("\n" + "id" + "\t | ");
		System.out.print("OperTime" + "\t\t | ");
		System.out.print("User" + "\t\t | ");
		System.out.print("Stat" + "\t | ");
		System.out.print("Type");
	}
	
	public static String getLoginFromId(int id) throws Exception{
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		PreparedStatement pst = bd.prepareStatement("SELECT User FROM "
				+ tableRequest + " WHERE id=?");
		pst.setInt(1, id);
		ResultSet resultSet = pst.executeQuery();
		String result = resultSet.getString(1);
		pst.close();
		resultSet.close();
		bd.close();		
		return result;
	}
	
	public static void setStatus(int id,String status)throws Exception{
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		PreparedStatement pst = bd.prepareStatement
				("UPDATE "+ tableRequest+" SET status=? WHERE id=?");
		pst.setString(1, status);
		pst.setInt(2,id);
		pst.executeUpdate();
		pst.close();
		bd.close();
	}

}
