package pak2111_infosite.utils;

import pak2111_infosite.model.*; 

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class DBLogOperations {

	static String dbPath = DBOperations.dbPath;
	static String dbName = DBOperations.dbName;
	static String tableLogs = "Logs";

	// Create Table
	public static void createLogsTable() throws Exception {
		Class.forName("org.sqlite.JDBC");
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS '"
				+ tableLogs
				+ "' (ID INTEGER PRIMARY KEY AUTOINCREMENT,Oper_Time Text,User Text,Type Text,Detail Text);");
		st.close();
		bd.close();
	}

	public static void insertLogToDB(String operTime, String user, String type,
			String detail) throws Exception {
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		PreparedStatement pst = bd.prepareStatement("INSERT INTO " + tableLogs
				+ " (Oper_Time,User,Type,Detail) values (?,?,?,?);");
		pst.setString(1, operTime);
		pst.setString(2, user);
		pst.setString(3, type);
		pst.setString(4, detail);
		pst.executeUpdate();
		pst.close();
		bd.close();
	}
	
	public static ArrayList<Log> getLogsDataFromDB() throws Exception {
		ArrayList<Log> getLogs = new ArrayList<Log>();
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		ResultSet ss = st.executeQuery("SELECT * FROM " + tableLogs+
				" WHERE detail<>'Enter' ORDER BY id DESC");
		while (ss.next()) {
			Log log = new Log(0,"", "","","");
			log.setId(ss.getInt(1));
			log.setOperTime(ss.getString(2));
			log.setUser(ss.getString(3));
			log.setType(ss.getString(4));
			log.setDetail(ss.getString(5));
			getLogs.add(log);
		}
		ss.close();
		st.close();
		bd.close();
		return getLogs;
	}
	
	public static void printLogsFromDB(ArrayList<Log> getLogs) throws Exception {
		printLogsTableCaption();
		System.out.println();
		Iterator<Log> iterator = getLogs.iterator();
		Log log = new Log(0,"", "","","");
		while (iterator.hasNext()) {
			log = (Log) (iterator.next());
			System.out.println(log);
		}
		System.out.println("TOTAL=" + DBOperations.lengthDB(tableLogs));
	}
	
	public static void printLogsTableCaption(){
		System.out.print("\n" + "id" + "\t | ");
		System.out.print("OperTime" + "\t\t | ");
		System.out.print("User" + "\t\t | ");
		System.out.print("Type" + "\t\t | ");
		System.out.print("Detail");
	}

}
