package pak2111_infosite.utils;

import pak2111_infosite.model.*; 

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class DBNewsOperations {
	
	static String dbPath	= DBOperations.dbPath;
	static String dbName	=DBOperations.dbName;
	static String tableNews	="News";

	// /JDBC REALIZATION
	// *************************News Entity Procedures
	// Create Table
	public static void createNewsTable() throws Exception {
		Class.forName("org.sqlite.JDBC");
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		st.execute("CREATE TABLE IF NOT EXISTS '"
				+ tableNews
				+ "' ('id' int,'title' text,'body' text,'user' text,'date' text);");
		st.close();
		bd.close();
	}

	public static void printNewsTableCaption() {
		System.out.print("\n" + "id" + "\t | ");
		System.out.print("Date" + "\t\t | ");
		System.out.print("User" + "\t\t | ");
		System.out.print("Title");
	}

	public static void showNewsDB() throws Exception {

		printNewsTableCaption();
		Connection bd=null;
		Statement st=null;
		ResultSet ss=null;
		try{
			bd=DriverManager.getConnection(dbPath + dbName);
			st = bd.createStatement();
			ss = st.executeQuery("SELECT * FROM " + tableNews);
			while (ss.next()) {
				System.out.print("\n" + ss.getString(1) + "\t | ");
				System.out.print(ss.getString(5) + "\t | ");
				System.out.print(ss.getString(4) + "\t | ");
				System.out.print(ss.getString(2));
			}
			System.out.println("\nTOTAL=" + DBOperations.lengthDB("News"));
		}catch(SQLException exception){
			System.out.println("Database Exception");
		}finally {
			if (ss!=null) ss.close();
			if (st!=null) st.close();
			if (bd!=null) bd.close();
		}
	}

	public static ArrayList<News> getNewsDataFromDB() throws Exception {
		ArrayList<News> getNews = new ArrayList<News>();
		Class.forName("org.sqlite.JDBC");
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		ResultSet ss = st.executeQuery("SELECT * FROM " + tableNews);
		while (ss.next()) {
			News news = new News(0, "", "", "", "");
			news.setId(ss.getInt(1));
			news.setNewsTitle(ss.getString(2));
			news.setAuthor(ss.getString(4));
			news.setNewsDate(ss.getString(5));
			getNews.add(news);
		}
		ss.close();
		st.close();
		bd.close();
		return getNews;
	}

	public static void printNewsFromDB(ArrayList<News> getNews) throws Exception {
		printNewsTableCaption();
		System.out.println();
		Iterator<News> iterator = getNews.iterator();
		News news = new News(0, "", "", "", "");
		while (iterator.hasNext()) {
			news = (News) (iterator.next());
			System.out.println(news);
		}
		System.out.println("TOTAL=" + DBOperations.lengthDB(tableNews));
	}

	public static void insertNewsToDB(int id, String title, String body, String user,
			String date) throws Exception {
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		PreparedStatement pst = bd.prepareStatement("INSERT INTO " + tableNews
				+ " values (?,?,?,?,?);");
		pst.setInt(1, id);
		pst.setString(2, title);
		pst.setString(3, body);
		pst.setString(4, user);
		pst.setString(5, date);
		pst.executeUpdate();
		pst.close();
		bd.close();
	}
	public static void deleteNewsID(int id)throws Exception{
		Connection bd=DriverManager.getConnection(dbPath+dbName);
		PreparedStatement pst=bd.prepareStatement("DELETE FROM "+tableNews+" WHERE id=?");
		pst.setInt(1,id);
		pst.executeUpdate();
		pst.close();
		bd.close();
	}
	
}
