package pak2111_infosite.utils;

import pak2111_infosite.model.*; 

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

public class DBUserOperations {

    static String dbPath = DBOperations.dbPath;
    static String dbName = DBOperations.dbName;
    static String tableUsers = "Users2";

    // /JDBC REALIZATION
    // *************************User Entity Procedures
    // Create Table
    public static void createUserTable() throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        Statement st = bd.createStatement();
        st.execute("CREATE TABLE IF NOT EXISTS '" + tableUsers
                + "' ('id' int,'user' text,'password' text,'role' text);");
        st.close();
        bd.close();
    }

    public static ArrayList<User> getUserDataFromDB() throws Exception {
        Class.forName("org.sqlite.JDBC");
        ArrayList<User> getUser = new ArrayList<User>();
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        Statement st = bd.createStatement();
        ResultSet ss = st.executeQuery("SELECT * FROM " + tableUsers);
        while (ss.next()) {
            User user = new User(0, "", "", "");
            user.setId(ss.getInt(1));
            user.setLogin(ss.getString(2));
            user.setPassword(ss.getString(3));
            user.setRole(ss.getString(4));
            getUser.add(user);
        }
        ss.close();
        st.close();
        bd.close();
        return getUser;
    }

    public static void printUsersFromDB(ArrayList<User> getUsers) throws Exception {
        printUsersTableCaption();
        System.out.println();
        Iterator<User> iterator = getUsers.iterator();
        User user = new User(0, "", "", "");
        while (iterator.hasNext()) {
            user = (User) (iterator.next());
            System.out.println(user);
        }
        System.out.println("TOTAL=" + DBOperations.lengthDB(tableUsers));
    }

    @Deprecated
    // Show Table Method
    public static void showUserDB() throws Exception {
        printUsersTableCaption();
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        Statement st = bd.createStatement();
        ResultSet ss = st.executeQuery("SELECT * FROM " + tableUsers);
        while (ss.next()) {
            System.out.print("\n" + ss.getRow() + "\t | ");
            // System.out.print(ss.getString(1) + "\t | ");
            System.out.print(ss.getString(2) + "\t | ");
            System.out.print(ss.getString(3) + "\t | ");
            System.out.print(ss.getString(4));
        }
        System.out.println("\nTOTAL=" + DBOperations.lengthDB("Users2"));
        ss.close();
        st.close();
        bd.close();
    }

    // Check User Password in DB
    public static String getPasswordForLogin(String loginFor) throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        PreparedStatement pst = bd.prepareStatement("SELECT password FROM "
                + tableUsers + " WHERE user=?");
        pst.setString(1, loginFor);
        ResultSet resultSet = pst.executeQuery();
        String result = resultSet.getString(1);
        pst.close();
        resultSet.close();
        bd.close();
        return result;
    }

    public static void changePassword(String login, String password) throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        PreparedStatement pst = bd.prepareStatement
                ("UPDATE " + tableUsers + " SET password=? WHERE user=?");
        pst.setString(1, password);
        pst.setString(2, login);
        pst.executeUpdate();
        pst.close();
        bd.close();
    }

    // Insert User Data to DB
    public static void insertUserToDB(int id, String login, String password, String role)
            throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        PreparedStatement pst = bd.prepareStatement("INSERT INTO " + tableUsers
                + " values (?,?,?,?);");
        pst.setInt(1, id);
        pst.setString(2, login);
        pst.setString(3, password);
        pst.setString(4, role);
        pst.executeUpdate();
        pst.close();
        bd.close();
    }

    public static void printUsersTableCaption() {
        System.out.print("\n" + "id" + "\t | ");
        System.out.print("Login" + "\t | ");
        System.out.print("Password" + "\t | ");
        System.out.print("Role");
    }

    public static String getRole(String login) throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        PreparedStatement pst = bd.prepareStatement("SELECT role FROM "
                + tableUsers + " WHERE user=?");
        pst.setString(1, login);
        ResultSet resultSet = pst.executeQuery();
        String result = resultSet.getString(1);
        pst.close();
        resultSet.close();
        bd.close();
        return result;
    }

    public static void deleteUserID(int id) throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        PreparedStatement pst = bd.prepareStatement("DELETE FROM " + tableUsers + " WHERE id=?");
        pst.setInt(1, id);
        pst.executeUpdate();
        pst.close();
        bd.close();
    }

    public static void setRole(String login, String role) throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        PreparedStatement pst = bd.prepareStatement
                ("UPDATE " + tableUsers + " SET role=? WHERE user=?");
        pst.setString(1, role);
        pst.setString(2, login);
        pst.executeUpdate();
        pst.close();
        bd.close();
    }

    public static int lengthDB() throws Exception {
        Connection bd = DriverManager.getConnection(dbPath + dbName);
        Statement st = bd.createStatement();
        ResultSet ss = st.executeQuery("SELECT count(*) FROM " + tableUsers);
        // System.out.print("\n Length of Database Figures2=" +
        // ss.getString(1));
        int r = Integer.parseInt(ss.getString(1));
        ss.close();
        st.close();
        bd.close();
        return r;
    }


}
