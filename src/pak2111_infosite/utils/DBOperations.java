package pak2111_infosite.utils;

import pak2111_infosite.model.*; 

import java.sql.*;

//import org.sqlite.SQLite;


public class DBOperations {

	// HOME DB PATH
	//	static String dbPath = "jdbc:sqlite:C:/Kirill/JAVA/";
	// JOB DB PATH
	static String dbPath ="jdbc:sqlite:C:/Kirill/SW/Java/workspace/DB/";
	static String dbName = "Figures.db";	

	// DB Length int method
	public static int lengthDB(String tableName) throws Exception {
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		ResultSet ss = st.executeQuery("SELECT count(*) FROM " + tableName);
		// System.out.print("\n Length of Database Figures2=" +
		// ss.getString(1));
		int r = Integer.parseInt(ss.getString(1));
		ss.close();
		st.close();
		bd.close();
		return r;
	}

	// ***************************************************************************

	public static void deleteAllFromDB(String tableName1) throws Exception {
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		st.execute("Delete FROM " + tableName1 + " WHERE id>=0;");
		st.close();
		bd.close();
	}

	// Execute Query
	static void queryBD(String sQuery) throws Exception {
		Connection bd = DriverManager.getConnection(dbPath + dbName);
		Statement st = bd.createStatement();
		ResultSet ss = st.executeQuery(sQuery);
		System.out.print("\n" + sQuery + " = " + ss.getString(1));
		ss.close();
		st.close();
		bd.close();
	}
}