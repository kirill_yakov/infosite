package pak2111_infosite.forms;

import pak2111_infosite.constants.MenuInstance;
import pak2111_infosite.constants.MessagesConstants;
import pak2111_infosite.model.News;
import pak2111_infosite.model.Session;
import pak2111_infosite.model.User;
import pak2111_infosite.utils.*;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class Forms implements MessagesConstants {

	static String tableUsers = "Users2";
	static String tableNews = "News";
	MenuInstance menuInstance;

	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_TODAY = "dd.MM.yyyy";

	static void formCaption(String formName, Session currentSession) {
		System.out.println(formName + "["
				+ currentSession.sessionUser.getLogin() + "]" + " : " + now());
	}

	public static String now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		return sdf.format(cal.getTime());
	}
	
	public static String dateNow(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_TODAY);
		return sdf.format(cal.getTime());
	}
	
	//Check commands on Request Form
	static String commandRequest(String line){
		String result=null;		
		if (line.matches("D[0-9]*|A[0-9]*|0"))
			result=line.substring(1, line.length());											
		else 
			System.out.println("Wrong format");
		return result;
	}

	public static void invitationMenuForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "MainMenu", "Enter");

		formCaption(WELCOME_FORM, currentSession);
		MenuInstance menuInstance=new MenuInstance();
		try{
			String role= DBUserOperations.getRole(currentSession.sessionUser.getLogin());
			menuInstance.setMenuInstance(role);
			System.out.println(menuInstance.printMenu());

		}catch(SQLException e){
			menuInstance.setMenuInstance(currentSession.sessionUser.getRole());
			System.out.println(menuInstance.printMenu());
		}

		System.out.println(COMMANDLINE_PLACE);
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();

		if (!menuInstance.checkPermission(Integer.parseInt(command))) {
			System.out.println(WRONG_COMMAND);
			invitationMenuForm(currentSession);
		}
		else {
			switch (command) {
				case "1": {
					newsListFormShort(currentSession);
					break;
				}
				case "2": {
					signInForm(currentSession);
					break;
				}
				case "3": {
					registerForm(currentSession);
					break;
				}
				case "4": {
					userList(currentSession);
					break;
				}
				case "6": {
					addNewsForm(currentSession);
					break;
				}
				case "8": {
					DBLogOperations.insertLogToDB(Forms.now(),
							currentSession.sessionUser.getLogin(), "MainMenu",
							"SignOff");
					System.out.println(SIGNOFF);
					invitationMenuForm(new Session());
					break;
				}
				case "9": {
					changePasswordForm(currentSession);
					break;
				}
				case "10": {
					requestAskForm(currentSession);
					break;
				}
				case "11": {
					DBLogOperations.insertLogToDB(Forms.now(),
							currentSession.sessionUser.getLogin(), "MainMenu", "Exit");
					System.out.println(BYE_MESSAGE);
					break;
				}
				case "12": {
					requestListForm(currentSession);
					break;
				}
				case "13": {
					actionsLogForm(currentSession);
					break;
				}
				case "-1": {
					DBOperations.deleteAllFromDB(tableNews);
					invitationMenuForm(currentSession);
					break;
				}
				case "-2": {
					DBOperations.deleteAllFromDB(tableUsers);
					invitationMenuForm(currentSession);
					break;
				}
				case "-3": {
					break;
				}
				default: {
					System.out.println(WRONG_COMMAND);
					invitationMenuForm(currentSession);
					break;
				}
			}
		}
	}
	
	static void signInForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "SignForm", "Enter");
		formCaption(SIGN_IN_FORM, currentSession);
		System.out.println(WELCOME_SIGN_IN);
		Scanner scanner = new Scanner(System.in);
		System.out.print(ENTER_LOGIN);
		String login = scanner.nextLine();
		System.out.print(ENTER_PASSWORD);
		String password = scanner.nextLine();
		try {
			if (DBUserOperations.getPasswordForLogin(login).equals(password)) {
				System.out.println(ACESS_GRANTED);
				User user = new User(DBOperations.lengthDB(tableUsers), login,
						password,"User");
				currentSession = new Session(user);
				DBLogOperations.insertLogToDB(Forms.now(),
						currentSession.sessionUser.getLogin(), "SignForm",
						"Access Granted");
				invitationMenuForm(currentSession);
			} else {
				System.out.println(WRONG_PASSWORD);
				DBLogOperations.insertLogToDB(Forms.now(),
						currentSession.sessionUser.getLogin(), "SignForm",
						"WrongPassword");
				invitationMenuForm(currentSession);
			}
		} catch (SQLException e) {
			System.out.println(WRONG_USER);
			DBLogOperations.insertLogToDB(Forms.now(),
					currentSession.sessionUser.getLogin(), "SignForm",
					"WrongUser");

			invitationMenuForm(currentSession);
		}
}

	static boolean checkLogin(String login) {
		boolean resultNumberOfChars = true;
		if ((login.length() <= 5) || (login.length() >= 11))
			resultNumberOfChars = false;
		boolean cases = true;
		boolean result = true;
		for (int i = 0; i < login.length(); i++) {
			Character ch = login.charAt(i);
			cases = ((i > 0) && (ch.isLowerCase(ch)))
					|| ((i == 0) && (ch.isUpperCase(ch)));
			if (!cases)
				result = false;
			if (!resultNumberOfChars)
				result = false;
		}
		if (!result)
			System.out.println(LOGIN_FAILED);
		return result;
	}

	static boolean checkPassword(String password) {
		boolean resultNumberOfChars = true;
		boolean result = true;
		if (password.length() != 4)
			resultNumberOfChars = false;
		for (int i = 0; i < password.length(); i++) {
			Character ch = password.charAt(i);
			if (!ch.isDigit(ch))
				result = false;
		}
		if (!resultNumberOfChars)
			result = false;
		if (!result)
			System.out.println(PASSWORD_FAILED);
		return result;
	}

	static void registerForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "RegForm", "Enter");
		formCaption(REGISTER_FORM, currentSession);
		System.out.println(MENU_LINK0);
		Scanner scanner = new Scanner(System.in);
		System.out.print(ENTER_LOGIN);
		String login = scanner.nextLine();
		System.out.print(ENTER_PASSWORD);
		String password = scanner.nextLine();
		try {
			if (DBUserOperations.getPasswordForLogin(login) != null)
				System.out.println(USER_EXIST);
			DBLogOperations.insertLogToDB(Forms.now(),
					currentSession.sessionUser.getLogin(), "RegForm",
					"User Exist");
			invitationMenuForm(currentSession);
		} catch (SQLException e) {
			if ((checkLogin(login)) & (checkPassword(password))) {
				System.out.println(USER_CREATED);
				User user = new User(DBOperations.lengthDB(tableUsers), login,
						password,"User");
				currentSession = new Session(user);
				DBUserOperations.insertUserToDB(
						DBOperations.lengthDB(tableUsers), login, password,"User");
				DBLogOperations.insertLogToDB(Forms.now(),
						currentSession.sessionUser.getLogin(), "RegForm",
						"User Created");
				invitationMenuForm(currentSession);
			} else {
				invitationMenuForm(currentSession);
			}
		}
	}

	static void userList(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "UserList", "Enter");
		formCaption(USER_LIST_FORM, currentSession);
		System.out.println(MENU_LINK0);
		boolean isAdmin=false;
		
		if (!currentSession.sessionUser.getLogin().equals("Anonymous")){
			String role=DBUserOperations.getRole(currentSession.sessionUser.getLogin());
			if (role.equals("Admin")) {
				isAdmin=true;
				System.out.println(">>> D5.Delete User with ID=5");				
			}
		}

		// OLD REALIZATION
		// DBUserOperations.showUserDB();
		// NEW REALIZATION - THROUGH COLLECTIONS
		DBUserOperations.printUsersFromDB(DBUserOperations.getUserDataFromDB());

		System.out.println(COMMANDLINE_PLACE);
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		switch (command.charAt(0)) {
		case '0': {
			invitationMenuForm(currentSession);
			break;
		}
		case 'D':{
			int id=Integer.parseInt(commandRequest(command));
			if ((isAdmin)&&(id!=0)){
				System.out.printf(">>>User with id=%d was deleted%n",id);
				DBLogOperations.insertLogToDB(Forms.now(),
						currentSession.sessionUser.getLogin(), "UserList", ("DeleteUser ID="+id));
				DBUserOperations.deleteUserID(id);			
			}
			userList(currentSession);
			break;
		}
		default: {
			System.out.println(WRONG_COMMAND);
			userList(currentSession);
			break;
		}
		}
	}

	static void newsListFormShort(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "NewsList", "Enter");
		formCaption(NEWS_LIST_FORM, currentSession);
		System.out.println(MENU_LINK0);
		boolean isAdmin=false;
		
		if (!currentSession.sessionUser.getLogin().equals("Anonymous")){
			String role=DBUserOperations.getRole(currentSession.sessionUser.getLogin());
			if (role.equals("Admin")) {
				isAdmin=true;
				System.out.println(">>> D5.Delete News with ID=5");							
			}
		}

		// OLD REALIZATION
		// DBOperations.showNewsDB();

		// NEW REALIZATION - THROUGH COLLECTIONS
		DBNewsOperations.printNewsFromDB(DBNewsOperations.getNewsDataFromDB());

		System.out.println(COMMANDLINE_PLACE);
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		switch (command.charAt(0)) {
		case '0': {
			invitationMenuForm(currentSession);
			break;
		}
		case 'D':{
			if (isAdmin){
				int id=Integer.parseInt(commandRequest(command));
				DBNewsOperations.deleteNewsID(id);			
				System.out.printf(">>>News with id=%d was deleted%n",id);
				DBLogOperations.insertLogToDB(Forms.now(),
						currentSession.sessionUser.getLogin(), "NewsList", ("DeleteNews ID="+id));
			}
			newsListFormShort(currentSession);
			break;
		}
		default: {
			System.out.println(WRONG_COMMAND);
			newsListFormShort(currentSession);
			break;
		}
		}
	}

	static void addNewsForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "NewsAdd", "Enter");
		formCaption(ADD_LIST_FORM, currentSession);
		Scanner scanner = new Scanner(System.in);
//		System.out.print(ENTER_NEWS_DATE);
//		String date = scanner.nextLine();
		String date=Forms.dateNow();
		System.out.print(ENTER_NEWS_TITLE);
		String title = scanner.nextLine();
		System.out.print(ENTER_NEWS_BODY);
		String body = scanner.nextLine();
		DBNewsOperations.insertNewsToDB(DBOperations.lengthDB("News"), title,
				body, currentSession.sessionUser.getLogin(), date);
		System.out.println(NEWS_ADDED);
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "NewsAdd", "NewsAdded");
		newsListFormShort(currentSession);
	}

	static void changePasswordForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "ChangePass", "Enter");
		formCaption(CHANGE_PASSWORD, currentSession);
		Scanner scanner = new Scanner(System.in);
		System.out.print(ENTER_OLD_PASSWORD);
		String oldPassword = scanner.nextLine();
		System.out.print(ENTER_NEW_PASSWORD);
		String newPassword = scanner.nextLine();
		System.out.print(CONFIRM_NEW_PASS);
		String confirmNewPassword = scanner.nextLine();
		String currentLogin = currentSession.sessionUser.getLogin();
		String password = DBUserOperations.getPasswordForLogin(currentLogin);
		if (!oldPassword.equals(password))
			System.out.println(WRONG_OLD_PASSWORD);
		if (!newPassword.equals(confirmNewPassword))
			System.out.println(NOT_CONFIRMED_PASS);
		if (newPassword.equals(password))
			System.out.println(NOT_SAME_PASS);
		if (checkPassword(newPassword) && oldPassword.equals(password)
				&& newPassword.equals(confirmNewPassword)
				&& (!newPassword.equals(password))) {
			System.out.println(PASS_CHANGED);
			DBUserOperations.changePassword(currentLogin, newPassword);
			DBLogOperations.insertLogToDB(Forms.now(),
					currentSession.sessionUser.getLogin(), "ChangePass",
					"PassChanged");
		}
		invitationMenuForm(currentSession);
	}

	static void actionsLogForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "ActionLog", "Enter");
		formCaption(ACTION_LOG_FORM, currentSession);
		System.out.println(MENU_LINK0);

		DBLogOperations.printLogsFromDB(DBLogOperations.getLogsDataFromDB());

		System.out.println(COMMANDLINE_PLACE);
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		switch (command) {
		case "0": {
			invitationMenuForm(currentSession);
			break;
		}
		default: {
			System.out.println(WRONG_COMMAND);
			userList(currentSession);
			break;
		}
		}
	}

	static void requestListForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "RequestList", "Enter");
		formCaption(REQUEST_LIST_FORM, currentSession);
		System.out.println(MENU_LINK0);
		System.out.println(">>> A5. Approve request with ID=5");
		System.out.println(">>> D4. Deny request with ID=4");
		
		DBRequestOperations.printRequestFromDB(DBRequestOperations.getRequestDataFromDB());

		System.out.println(COMMANDLINE_PLACE);
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		switch (command.charAt(0)) {
		case '0': {
			invitationMenuForm(currentSession);
			break;
		}
		case 'A':{				
			int id=Integer.parseInt(commandRequest(command));
			String login=DBRequestOperations.getLoginFromId(id);
			DBUserOperations.setRole(login,"Manager");			
			DBRequestOperations.setStatus(id,"Approved");
			
			System.out.printf(">>>Request with id=%d was approved%n",id);
			DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "RequestList", ("ApproveRequest ID="+id));			
			requestListForm(currentSession);
			break;
		}
		case 'D':{
			int id=Integer.parseInt(commandRequest(command));
			DBRequestOperations.setStatus(id,"Denied");
			System.out.printf(">>>Request with id=%d was denied%n",id);
			DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "RequestList", ("DenyRequest ID="+id));			
			requestListForm(currentSession);
			
			break;
		}								
		default: {
			System.out.println(WRONG_COMMAND);
			userList(currentSession);
			break;
		}
		}
	}
	
	static void requestAskForm(Session currentSession) throws Exception {
		DBLogOperations.insertLogToDB(Forms.now(),
				currentSession.sessionUser.getLogin(), "RequestAsk", "Enter");
		formCaption(REQUEST_ASK_FORM, currentSession);
		System.out.println(MENU_LINK0);
		
		System.out.println(">>>You have asked Role - Content Manager");
		DBRequestOperations.insertRequestToDB(Forms.now(), currentSession.sessionUser.getLogin(),
				"Waiting", "Content Manager");
		
		System.out.println(COMMANDLINE_PLACE);
		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		switch (command) {
		case "0": {
			invitationMenuForm(currentSession);
			break;
		}
		default: {
			System.out.println(WRONG_COMMAND);
			userList(currentSession);
			break;
		}
		}
	}
	
	//�������� ������� ����, �������� ������� ������ �� ��, 
	//��������� ��� ������ ���������, ����������
	//��������� ������� ������ �� ���������, ��������� ������� �� ����������.

}