package pak2111_infosite.constants;

public interface MessagesConstants {

    final String MENU_LINK0 = ">>> 0.Menu";
    final String MENU_LINK1_NEWS_LIST = ">>> 1.News List";
    final String MENU_LINK2_SIGNIN = ">>> 2.Sign In";
    final String MENU_LINK3_REGISTER = ">>> 3.Register";
    final String MENU_LINK4_USERS_LIST = ">>> 4.Users List";
    final String MENU_LINK5_NEWS_DETAIL = ">>> 5.Go to Detail News";
    final String MENU_LINK6_ADD_NEWS = ">>> 6.Add News";
    final String MENU_LINK8_SIGN_OFF = ">>> 8.SignOff";
    final String MENU_LINK9_CH_PASS = ">>> 9.ChangePass";
    final String MENU_LINK10_ASK_CM = ">>> 10.Ask Content Manager";
    final String MENU_LINK11_EXIT = ">>> 11.Exit";
    final String MENU_LINK12_REQ_APP = ">>> 12.Requests Approval";
    final String MENU_LINK13_ACTIONS = ">>> 13.Actions Log";



    final String COMMANDLINE_PLACE = "---command---line---";
    final String ENTER_LOGIN = ">>> Enter Login   :";
    final String ENTER_PASSWORD = ">>> Enter Password:";
    final String WRONG_COMMAND = ">>> Wrong Command";
    final String SIGNOFF = ">>> User signed Off";
    final String BYE_MESSAGE = ">>> Come again.Buy";

    //SIGN IN FORM
    final String WELCOME_SIGN_IN = ">>> Please, Enter your User Credentials";
    final String ACESS_GRANTED = ">>> Access granted";
    final String WRONG_PASSWORD = ">>> Wrong password. Access denied";
    final String WRONG_USER = ">>> Wrong user. Login doesn't exist";

    //REGISTER FORM
    final String USER_EXIST = ">>> User Exist";
    final String USER_CREATED = ">>> User Created";
    final String LOGIN_FAILED = ">>> Login failed. It can contain 6-10 letters, " +
            "starts from Upper(A), example Kirill";
    final String PASSWORD_FAILED = ">>> Password failed. It can contain 4 numbers, " +
            "example 1303";

    //ADD NEWS FORM
    final String ENTER_NEWS_DATE = ">>> Date	:";
    final String ENTER_NEWS_TITLE = ">>> Title	:";
    final String ENTER_NEWS_BODY = ">>> Body	:";
    final String NEWS_ADDED = ">>> News Added";

    //CHANGE PASSWORD
    final String ENTER_OLD_PASSWORD = ">>> Enter Old Password: ";
    final String ENTER_NEW_PASSWORD = ">>> Enter New Password: ";
    final String CONFIRM_NEW_PASS = ">>> Confirm New Password: ";
    final String WRONG_OLD_PASSWORD = ">>> You've typed wrong old password";
    final String NOT_CONFIRMED_PASS = ">>> New Password hasn't confirmed";
    final String NOT_SAME_PASS = ">>> New Password shouldn't be the same as old";
    final String PASS_CHANGED = ">>> Password successfully changed";

    //FORMS
    final String WELCOME_FORM = "******************** Welcome to IT News Site ********************";
    final String NEWS_LIST_FORM = "********************        News List        ********************";
    final String SIGN_IN_FORM = "********************       SignIn Form       ********************";
    final String REGISTER_FORM = "********************      Register Form      ********************";
    final String USER_LIST_FORM = "********************       User List         ********************";
    final String ADD_LIST_FORM = "********************         Add News        ********************";
    final String CHANGE_PASSWORD = "********************     Change Password     ********************";
    final String ACTION_LOG_FORM = "********************     Action Log List     ********************";
    final String REQUEST_LIST_FORM = "*********************      Request List     *********************";
    final String REQUEST_ASK_FORM = "***********************      Ask Role      **********************";

}
