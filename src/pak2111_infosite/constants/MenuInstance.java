package pak2111_infosite.constants;

import java.util.Arrays;
import java.util.HashMap;

public class MenuInstance implements MessagesConstants {

    private String userRole;
    private int[]menuConfig;

    final static int[]allPermissions=      {1,2,3,4,6,8,9,10,11,12,13};
    final static int[]adminPermissions=    {1,    4,6,8,9,11,   12,13};
    final static int[]managerPermissions=  {1,      6,8,9,   11};
    final static int[]userPermissions=     {1,        8,9,10,11};
    final static int[]anonymousPermissions={1,2,3,           11};

    final static String[] MENU = {"",MENU_LINK1_NEWS_LIST, MENU_LINK2_SIGNIN, MENU_LINK3_REGISTER,
            MENU_LINK4_USERS_LIST,"", MENU_LINK6_ADD_NEWS,"", MENU_LINK8_SIGN_OFF, MENU_LINK9_CH_PASS,
            MENU_LINK10_ASK_CM, MENU_LINK11_EXIT, MENU_LINK12_REQ_APP, MENU_LINK13_ACTIONS};

    final static HashMap<String,int[]> roleConfig=new HashMap<>();{
        roleConfig.put("Admin",adminPermissions);
        roleConfig.put("Manager",managerPermissions);
        roleConfig.put("User",userPermissions);
        roleConfig.put("Anonymous",anonymousPermissions);
        roleConfig.put("All",allPermissions);
    }
    public MenuInstance(){
    }

    public void setMenuInstance(String roleName){
        this.userRole=roleName;
        menuConfig=roleConfig.get(roleName);
    }

    public boolean checkPermission(int command){
        boolean result=false;
        if ((Arrays.binarySearch(roleConfig.get(userRole), command))>=0) result=true;
        return result;
    }

    public String printMenu(){
        String result="";
        for (int i :menuConfig) {
            result=result+MENU[i]+"\n";
        }
        return result;
    }

    @Override
    public String toString() {
        String result="";
        for (String iterator :MENU) {
            result=result+iterator+"\n";
        }
        return result;
    }
}
