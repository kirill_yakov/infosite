package pak01_core;

import java.util.ArrayList;
import java.util.Iterator;

public class Numbers1 {
    //----------------------------------------------
    public static ArrayList multiplesForNumber(int number) {
        ArrayList result = new ArrayList();
        for (int i = 1; i < number; i++) {
            if (number % i == 0) result.add(i);
        }
        return result;
    }
    //----------------------------------------------
    public static boolean isNumberEasy(int number) {
        boolean result = true;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                result = false;
            }
        }
        return result;
    }
    //----------------------------------------------
    public static ArrayList easyNumbers(int number) {
        ArrayList result = new ArrayList();
        for (int i = 1; i <= number; i++) {
            if (isNumberEasy(i)) result.add(i);
        }
        return result;
    }
    //----------------------------------------------
    public static int sumOfMultiplesForNumber(int number) {
        int result = 0;
        ArrayList arrayList = multiplesForNumber(number);
        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            result = result + (Integer) (iterator.next());
        }
        return result;
    }
    //----------------------------------------------
    public static ArrayList getPerfectNumbers(int number) {
        ArrayList result = new ArrayList();
        for (int i = 2; i <= number; i++) {
            if (i == sumOfMultiplesForNumber(i)) {
                System.out.println(i + "|" + multiplesForNumber(i));
                result.add(i);
            }
        }
        return result;
    }
    //----------------------------------------------
    //----------------------------------------------
    //----------------------------------------------
    //----------------------------------------------
    //----------------------------------------------
    //----------------------------------------------
    public static void main(String[] args) {

//        System.out.println(easyNumbers(100));
//        System.out.println(multiplesForNumber(100));
//        System.out.println(sumOfMultiplesForNumber(28));
//        System.out.println(getPerfectNumbers(10000));


    }
}
